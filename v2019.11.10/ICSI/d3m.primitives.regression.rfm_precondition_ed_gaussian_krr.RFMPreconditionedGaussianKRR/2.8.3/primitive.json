{
    "id": "90d9eefc-2db3-4738-a0e7-72eedab2d93a",
    "version": "2.8.3",
    "name": "RFM Preconditioned Gaussian Kernel Ridge Regression",
    "description": "Performs gaussian kernel regression using a random feature map to precondition the\nproblem for faster convergence:\nforms the kernel\n    K_{ij} = exp(-||x_i - x_j||^2/(2sigma^2))\nand solves\n    alphahat = argmin ||K alpha - y||_F^2 + lambda ||alpha||_F^2\npredictions are then formed by\n    ypred = K(trainingData, x) alphahat\n\nAttributes\n----------\nmetadata : PrimitiveMetadata\n    Primitive's metadata. Available as a class attribute.\nlogger : Logger\n    Primitive's logger. Available as a class attribute.\nhyperparams : Hyperparams\n    Hyperparams passed to the constructor.\nrandom_seed : int\n    Random seed passed to the constructor.\ndocker_containers : Dict[str, DockerContainer]\n    A dict mapping Docker image keys from primitive's metadata to (named) tuples containing\n    container's address under which the container is accessible by the primitive, and a\n    dict mapping exposed ports to ports on that address.\nvolumes : Dict[str, str]\n    A dict mapping volume keys from primitive's metadata to file and directory paths\n    where downloaded and extracted files are available to the primitive.\ntemporary_directory : str\n    An absolute path to a temporary directory a primitive can use to store any files\n    for the duration of the current pipeline run phase. Directory is automatically\n    cleaned up after the current pipeline run phase finishes.",
    "python_path": "d3m.primitives.regression.rfm_precondition_ed_gaussian_krr.RFMPreconditionedGaussianKRR",
    "primitive_family": "REGRESSION",
    "algorithm_types": [
        "KERNEL_METHOD",
        "MULTIVARIATE_REGRESSION"
    ],
    "keywords": [
        "kernel learning",
        "kernel ridge regression",
        "preconditioned CG",
        "Gaussian",
        "RBF",
        "regression"
    ],
    "source": {
        "name": "ICSI",
        "contact": "mailto:gittea@rpi.edu",
        "uris": [
            "https://github.com/ICSI-RealML/realML.git"
        ]
    },
    "installation": [
        {
            "type": "PIP",
            "package_uri": "git+https://github.com/ICSI-RealML/realML.git@e298e75e2274ca6e51ce871405e8f3a6edbcbe18#egg=realML"
        }
    ],
    "location_uris": [
        "https://github.com/ICSI-RealML/realML/blob/master/realML/kernel/RFMPreconditionedGaussianKRRSolver.py"
    ],
    "preconditions": [
        "NO_MISSING_VALUES",
        "NO_CATEGORICAL_VALUES"
    ],
    "schema": "https://metadata.datadrivendiscovery.org/schemas/v0/primitive.json",
    "original_python_path": "realML.kernel.RFMPreconditionedGaussianKRRSolver.RFMPreconditionedGaussianKRR",
    "primitive_code": {
        "class_type_arguments": {
            "Inputs": "d3m.container.numpy.ndarray",
            "Outputs": "d3m.container.numpy.ndarray",
            "Params": "realML.kernel.RFMPreconditionedGaussianKRRSolver.Params",
            "Hyperparams": "realML.kernel.RFMPreconditionedGaussianKRRSolver.Hyperparams"
        },
        "interfaces_version": "2019.11.10",
        "interfaces": [
            "supervised_learning.SupervisedLearnerPrimitiveBase",
            "base.PrimitiveBase"
        ],
        "hyperparams": {
            "lparam": {
                "type": "d3m.metadata.hyperparams.LogUniform",
                "default": 0.01,
                "structural_type": "float",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/TuningParameter"
                ],
                "description": "l2 regularization to use for the kernel regression",
                "lower": 0.0001,
                "upper": 1000,
                "lower_inclusive": true,
                "upper_inclusive": false
            },
            "sigma": {
                "type": "d3m.metadata.hyperparams.LogUniform",
                "default": 0.01,
                "structural_type": "float",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/TuningParameter"
                ],
                "description": "bandwidth (sigma) parameter for the kernel regression",
                "lower": 0.0001,
                "upper": 1000,
                "lower_inclusive": true,
                "upper_inclusive": false
            },
            "eps": {
                "type": "d3m.metadata.hyperparams.LogUniform",
                "default": 0.0001,
                "structural_type": "float",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "relative error stopping tolerance for PCG solver",
                "lower": 1e-14,
                "upper": 0.01,
                "lower_inclusive": true,
                "upper_inclusive": false
            },
            "maxIters": {
                "type": "d3m.metadata.hyperparams.UniformInt",
                "default": 200,
                "structural_type": "int",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/TuningParameter"
                ],
                "description": "maximum iterations of PCG",
                "lower": 50,
                "upper": 500,
                "lower_inclusive": true,
                "upper_inclusive": false
            }
        },
        "arguments": {
            "hyperparams": {
                "type": "realML.kernel.RFMPreconditionedGaussianKRRSolver.Hyperparams",
                "kind": "RUNTIME"
            },
            "random_seed": {
                "type": "int",
                "kind": "RUNTIME",
                "default": 0
            },
            "docker_containers": {
                "type": "typing.Union[NoneType, typing.Dict[str, d3m.primitive_interfaces.base.DockerContainer]]",
                "kind": "RUNTIME",
                "default": null
            },
            "timeout": {
                "type": "typing.Union[NoneType, float]",
                "kind": "RUNTIME",
                "default": null
            },
            "iterations": {
                "type": "typing.Union[NoneType, int]",
                "kind": "RUNTIME",
                "default": null
            },
            "produce_methods": {
                "type": "typing.Sequence[str]",
                "kind": "RUNTIME"
            },
            "inputs": {
                "type": "d3m.container.numpy.ndarray",
                "kind": "PIPELINE"
            },
            "outputs": {
                "type": "d3m.container.numpy.ndarray",
                "kind": "PIPELINE"
            },
            "params": {
                "type": "realML.kernel.RFMPreconditionedGaussianKRRSolver.Params",
                "kind": "RUNTIME"
            }
        },
        "class_methods": {},
        "instance_methods": {
            "__init__": {
                "kind": "OTHER",
                "arguments": [
                    "hyperparams",
                    "random_seed",
                    "docker_containers"
                ],
                "returns": "NoneType",
                "description": "Initializes the preconditioned gaussian kernel ridge regression primitive."
            },
            "fit": {
                "kind": "OTHER",
                "arguments": [
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.CallResult[NoneType]",
                "description": "Learns the kernel regression coefficients alpha given training pairs (X,y)\n\nParameters\n----------\ntimeout : float\n    A maximum time this primitive should be fitting during this method call, in seconds.\niterations : int\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nCallResult[None]\n    A ``CallResult`` with ``None`` value."
            },
            "fit_multi_produce": {
                "kind": "OTHER",
                "arguments": [
                    "produce_methods",
                    "inputs",
                    "outputs",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.MultiCallResult",
                "description": "A method calling ``fit`` and after that multiple produce methods at once.\n\nThis method allows primitive author to implement an optimized version of both fitting\nand producing a primitive on same data.\n\nIf any additional method arguments are added to primitive's ``set_training_data`` method\nor produce method(s), or removed from them, they have to be added to or removed from this\nmethod as well. This method should accept an union of all arguments accepted by primitive's\n``set_training_data`` method and produce method(s) and then use them accordingly when\ncomputing results.\n\nThe default implementation of this method just calls first ``set_training_data`` method,\n``fit`` method, and all produce methods listed in ``produce_methods`` in order and is\npotentially inefficient.\n\nParameters\n----------\nproduce_methods : Sequence[str]\n    A list of names of produce methods to call.\ninputs : Inputs\n    The inputs given to ``set_training_data`` and all produce methods.\noutputs : Outputs\n    The outputs given to ``set_training_data``.\ntimeout : float\n    A maximum time this primitive should take to both fit the primitive and produce outputs\n    for all produce methods listed in ``produce_methods`` argument, in seconds.\niterations : int\n    How many of internal iterations should the primitive do for both fitting and producing\n    outputs of all produce methods.\n\nReturns\n-------\nMultiCallResult\n    A dict of values for each produce method wrapped inside ``MultiCallResult``."
            },
            "get_params": {
                "kind": "OTHER",
                "arguments": [],
                "returns": "realML.kernel.RFMPreconditionedGaussianKRRSolver.Params",
                "description": "Returns parameters of this primitive.\n\nParameters are all parameters of the primitive which can potentially change during a life-time of\na primitive. Parameters which cannot are passed through constructor.\n\nParameters should include all data which is necessary to create a new instance of this primitive\nbehaving exactly the same as this instance, when the new instance is created by passing the same\nparameters to the class constructor and calling ``set_params``.\n\nNo other arguments to the method are allowed (except for private arguments).\n\nReturns\n-------\nParams\n    An instance of parameters."
            },
            "multi_produce": {
                "kind": "OTHER",
                "arguments": [
                    "produce_methods",
                    "inputs",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.MultiCallResult",
                "description": "A method calling multiple produce methods at once.\n\nWhen a primitive has multiple produce methods it is common that they might compute the\nsame internal results for same inputs but return different representations of those results.\nIf caller is interested in multiple of those representations, calling multiple produce\nmethods might lead to recomputing same internal results multiple times. To address this,\nthis method allows primitive author to implement an optimized version which computes\ninternal results only once for multiple calls of produce methods, but return those different\nrepresentations.\n\nIf any additional method arguments are added to primitive's produce method(s), they have\nto be added to this method as well. This method should accept an union of all arguments\naccepted by primitive's produce method(s) and then use them accordingly when computing\nresults.\n\nThe default implementation of this method just calls all produce methods listed in\n``produce_methods`` in order and is potentially inefficient.\n\nIf primitive should have been fitted before calling this method, but it has not been,\nprimitive should raise a ``PrimitiveNotFittedError`` exception.\n\nParameters\n----------\nproduce_methods : Sequence[str]\n    A list of names of produce methods to call.\ninputs : Inputs\n    The inputs given to all produce methods.\ntimeout : float\n    A maximum time this primitive should take to produce outputs for all produce methods\n    listed in ``produce_methods`` argument, in seconds.\niterations : int\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nMultiCallResult\n    A dict of values for each produce method wrapped inside ``MultiCallResult``."
            },
            "produce": {
                "kind": "PRODUCE",
                "arguments": [
                    "inputs",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.CallResult[d3m.container.numpy.ndarray]",
                "singleton": false,
                "inputs_across_samples": [],
                "description": "Predict the value for each sample in X\n\nInputs:\n    X: array of shape [n_samples, n_features]\nOutputs:\n    y: array of shape [n_samples, n_targets]\n\nParameters\n----------\ninputs : Inputs\n    The inputs of shape [num_inputs, ...].\ntimeout : float\n    A maximum time this primitive should take to produce outputs during this method call, in seconds.\niterations : int\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nCallResult[Outputs]\n    The outputs of shape [num_inputs, ...] wrapped inside ``CallResult``."
            },
            "set_params": {
                "kind": "OTHER",
                "arguments": [
                    "params"
                ],
                "returns": "NoneType",
                "description": "Sets parameters of this primitive.\n\nParameters are all parameters of the primitive which can potentially change during a life-time of\na primitive. Parameters which cannot are passed through constructor.\n\nNo other arguments to the method are allowed (except for private arguments).\n\nParameters\n----------\nparams : Params\n    An instance of parameters."
            },
            "set_training_data": {
                "kind": "OTHER",
                "arguments": [
                    "inputs",
                    "outputs"
                ],
                "returns": "NoneType",
                "description": "Sets the training data:\n    Input: array, shape = [n_samples, n_features]\n    Output: array, shape = [n_samples, n_targets]\nOnly uses one input and output\n\nParameters\n----------\ninputs : Inputs\n    The inputs.\noutputs : Outputs\n    The outputs."
            }
        },
        "class_attributes": {
            "logger": "logging.Logger",
            "metadata": "d3m.metadata.base.PrimitiveMetadata"
        },
        "instance_attributes": {
            "hyperparams": "d3m.metadata.hyperparams.Hyperparams",
            "random_seed": "int",
            "docker_containers": "typing.Dict[str, d3m.primitive_interfaces.base.DockerContainer]",
            "volumes": "typing.Dict[str, str]",
            "temporary_directory": "typing.Union[NoneType, str]"
        },
        "params": {
            "exemplars": "typing.Union[NoneType, numpy.ndarray]",
            "coeffs": "typing.Union[NoneType, numpy.ndarray]"
        }
    },
    "structural_type": "realML.kernel.RFMPreconditionedGaussianKRRSolver.RFMPreconditionedGaussianKRR",
    "digest": "031585496ba64f9bf8cfd20f1fa113ebceca2ea4e4e68f79e5cd1dbc8fe1dac7"
}
